<?php
namespace Maowenke\RedisForLock;
use think\facade\Cache;

class RedisLock
{
    public static function getLock(string $key='lock',int $lockExpire=10,bool $untilGet=false,int $usleep = 500000){
        $vk = 'RedisLock'.$key;
        $redis = Cache::store('redis');
        $value = $redis->get($vk);
        $lockValue = time()+$lockExpire;
        if(empty($value)){
            $bool = $redis->setnx($vk,$lockValue);
            if($bool||($redis->get($vk) < time() && $redis->getSet($vk, $lockValue) < time() )){
//                if($redis->ttl($vk)){
//                    $redis->del($vk);
//                }
                return true;
            }else{
                if($untilGet){
                    usleep($usleep);
                    return self::getLock($key,$lockExpire,$usleep);
                }else{
                    return false;
                }
            }
        }else{
            if($redis->get($vk) < time() && $redis->getSet($vk, $lockValue) < time() ){
//                if($redis->ttl($vk)){
//                    $redis->del($vk);
//                }
                return true;
            }
            if($untilGet){
                usleep($usleep);
                return self::getLock($key,$lockExpire,$usleep);
            }
            return false;
        }
        return true;
    }
    public static function releaseLock(string $key='lock'){
        $vk = 'RedisLock'.$key;
        $redis =  Cache::store('redis');
        return $redis->expire($vk,0);
    }
}